## Instructions for Minikube (OS X)

## Install kubectl if you do not have it (download + put it in your path)
```
curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/darwin/amd64/kubectl
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
```

## Install minikube 
```
curl -Lo minikube https://storage.googleapis.com/minikube/releases/v0.16.0/minikube-darwin-amd64 && chmod +x minikube && sudo mv minikube /usr/local/bin/
```

## Install xhyve
```
brew install docker-machine-driver-xhyve
```
(Follow completion steps)

## Tell minikube to use xhyve
```
minikube config set vm-driver xhyve
```

## Start minikube
```
minikube start
```

## Open k8s dashboard
```
minikube dashboard
```

## Load Prometheus
```
kubectl apply -f prometheus-configmap.yml
kubectl apply -f prometheus-svc.yml
kubectl apply -f prometheus-deployment.yml
```

## Get Prometheus Service URL, browse to it to ensure it's working
```
minikube service --url prometheus
```

## Tell GDK to listen to more than localhost. (Make sure you are in root GDK dir)
```
echo 0.0.0.0 > host
```

## Edit GDK to use it’s real IP not localhost. 
Edit `config/gitlab.yml` and change `host:` to your IP

Restart GDK

## Also note you should now access GitLab by the external URL (e.g. 192.168.1.1 not localhost)
Otherwise weird things with application.js happen

## Edit `gitlab-runner-docker-configmap.yml`
Replace the URL with your computer’s IP address and GDK port (3000) (IP CAN’T BE 127.0.0.1).

Replace registration token with token for your project. Note if the token contains uppercase characters, it will fail due to a bug in the Runner. You can manually set the CI/CD token further down the CI/CD settings screen, set it to all lowercase letters and numbers.

## Deploy GitLab Runner - can view Pod logs to confirm it registered
```
kubectl apply -f gitlab-runner-docker-configmap.yml
kubectl apply -f gitlab-runner-docker-deployment.yml 
```

## Import Project
Import from https://gitlab.com/joshlambert/hello-world.git

## Run a pipeline to kick off deploy.
Can validate it worked by looking at k8s dashboard, or accessing URL. If the deploy failed, you may need to edit the project's runner token to not include capital letters.
```
minikube service production
```

## Configure Prometheus Service Integration
Go into Project Settings, Integrations. Select Prometheus.

Enter the Prometheus URL from the step above, and click Active. Save and test.

## Go to Pipelines, Environments, then click on an Environment. Should see a blank button at top right.